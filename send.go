package bitwig

import (
	//	"fmt"

	"gitlab.com/goosc/osc"
)

const (
	// Arg: int (0-4)
	PreRoll osc.I = "/preroll"

	Undo osc.NoArg = "/undo"
	Redo osc.NoArg = "/redo"

	// Arg: BPM (int)
	TempoRaw osc.I = "/tempo/raw"

	TempoTap osc.NoArg = "/tempo/tap"

	// fill with
	// +     -> next
	// -     -> previous
	// ++    -> fast forward
	// --    -> rewind
	Position osc.NoArg = "/position/%s"

	// fill with
	// +     -> next
	// -     -> previous
	Project osc.NoArg = "/project/%s"

	// int(1=on,0=off)
	ProjectEngine Switch = "/project/engine"

	Click osc.NoArg = "/click"

	// Arg int: 1
	Stop On = "/stop"

	Play osc.Toggle = "/play"

	// Arg int: 1
	Restart On = "/restart"

	// int(1)
	Repeat On = "/repeat"

	PunchIn         osc.Toggle = "/punchIn"
	PunchOut        osc.Toggle = "/punchOut"
	Record          osc.NoArg  = "/record"
	Overdub         osc.Toggle = "/overdub"
	OverdubLauncher osc.Toggle = "/overdub/launcher"

	// int(n)
	Crossfade osc.I = "/crossfade"

	Autowrite       osc.I      = "/autowrite"
	AutowriteToggle osc.Toggle = "/autowrite"

	// string("latch"/"touch"/"write")
	AutomationWriteMode osc.S = "/automationWriteMode"

	LayoutArrange osc.NoArg = "/layout/arrange"
	LayoutMix     osc.NoArg = "/layout/mix"
	LayoutEdit    osc.NoArg = "/layout/edit"

	PanelNoteEditor       osc.NoArg = "/panel/noteEditor"
	PanelAutomationEditor osc.NoArg = "/panel/automationEditor"
	PanelDevices          osc.NoArg = "/panel/devices"
	PanelMixer            osc.NoArg = "/panel/mixer"
	PanelFullscreen       osc.NoArg = "/panel/fullscreen"

	ArrangerCueMarkerVisibility           osc.NoArg = "/arranger/cueMarkerVisibility"
	ArrangerPlaybackFollow                osc.NoArg = "/arranger/playbackFollow"
	ArrangerTrackRowHeight                osc.NoArg = "/arranger/trackRowHeight"
	ArrangerClipLauncherSectionVisibility osc.NoArg = "/arranger/clipLauncherSectionVisibility"
	ArrangerTimeLineVisibility            osc.NoArg = "/arranger/timeLineVisibility"
	ArrangerIOSectionVisibility           osc.NoArg = "/arranger/ioSectionVisibility"
	ArrangerEffectTracksVisibility        osc.NoArg = "/arranger/effectTracksVisibility"
	MixerClipLauncherSectionVisibility    osc.NoArg = "/mixer/clipLauncherSectionVisibility"
	MixerCrossFadeSectionVisibility       osc.NoArg = "/mixer/crossFadeSectionVisibility"
	MixerDeviceSectionVisibility          osc.NoArg = "/mixer/deviceSectionVisibility"
	MixerSendsSectionVisibility           osc.NoArg = "/mixer/sendsSectionVisibility"
	MixerIOSectionVisibility              osc.NoArg = "/mixer/ioSectionVisibility"
	MixerMeterSectionVisibility           osc.NoArg = "/mixer/meterSectionVisibility"

	TrackBankNext     osc.NoArg = "/track/bank/+"
	TrackBankPrevious osc.NoArg = "/track/bank/-"

	// Page = 8
	TrackBankPageNext     osc.NoArg = "/track/bank/page/+"
	TrackBankPagePrevious osc.NoArg = "/track/bank/page/-"

	// track is 1-8; red, green, blue between 0 and 1
	// float(red), float(green), float(blue)
	TrackColor osc.FFF = "/track/%d/color"

	TrackNext     osc.NoArg = "/track/+"
	TrackPrevious osc.NoArg = "/track/-"

	// VU-Meter notifications
	// int(1=on,0=off)
	TrackVU Switch = "/track/vu"

	// Toggles between the Audio/Instrument and Effect track bank
	TrackToggleBank    osc.NoArg = "/track/toggleBank"
	TrackAddAudio      osc.NoArg = "/track/add/audio"
	TrackAddEffect     osc.NoArg = "/track/add/effect"
	TrackAddInstrument osc.NoArg = "/track/add/instrument"

	// int(1=on,0=off)
	TrackActivated Switch = "/track/%d/activated"

	TrackSelect osc.NoArg = "/track/%d/select"

	// int(vol)
	TrackVolume osc.I = "/track/%d/volume"

	// int(1=on,0=off)
	TrackVolumeIndicate Switch = "/track/%d/volume/indicate"

	// int(pan)
	TrackPan osc.I = "/track/%d/pan"

	// int(1=on,0=off)
	TrackPanIndicate Switch = "/track/%d/pan/indicate"

	TrackMute osc.Toggle = "/track/%d/mute"
	TrackSolo osc.Toggle = "/track/%d/solo"

	// int(1=on,0=off)
	TrackRecArm       Switch     = "/track/%d/recarm"
	TrackRecArmToggle osc.Toggle = "/track/%d/recarm"

	TrackMonitor     osc.Toggle = "/track/%d/monitor"
	TrackMonitorAuto osc.Toggle = "/track/%d/monitor/auto"

	// int(1)
	TrackCrossfadeModeA On = "/track/%d/crossfadeMode/A"

	// int(1)
	TrackCrossfadeModeB On = "/track/%d/crossfadeMode/B"

	// int(1)
	TrackCrossfadeModeAB On = "/track/%d/crossfadeMode/AB"

	// int(vol)
	TrackSendVolume osc.I = "/track/%d/send/%d/volume"

	// int(1=on,0=off)
	TrackSendVolumeIndicate Switch = "/track/%d/send/%d/volume/indicate"

	TrackClipLaunch On = "/track/%v/clip/%v/launch"
	TrackClipRecord On = "/track/%v/clip/%v/record"
	TrackClipSelect On = "/track/%v/clip/%v/select"
	TrackClipStop   On = "/track/%v/clip/stop"

	TrackClipReturnToArrangement osc.Toggle = "/track/%v/clip/returntoarrangement"
	TrackStop                    On         = "/track/stop"

	// int(1=on,0=off)
	TrackIndicateVolume Switch = "/track/indicate/volume"

	// int(1=on,0=off)
	TrackIndicatePan Switch = "/track/indicate/pan"

	// int(1=on,0=off)
	TrackIndicateSend Switch = "/track/indicate/send/%d"

	// int(1=on,0=off)
	MasterIndicateVolume Switch = "/master/indicate/volume"

	// int(1=on,0=off)
	MasterIndicatePan Switch = "/master/indicate/pan"

	SceneLaunch On = "/scene/%v/launch"

	SceneNext     osc.NoArg = "/scene/+"
	ScenePrevious osc.NoArg = "/scene/-"

	// Bank = 8
	SceneBankNext osc.NoArg = "/scene/bank/+"

	SceneBankPrevious osc.NoArg = "/scene/bank/-"

	// Group actions
	TrackEnter  osc.NoArg = "/track/%d/enter"
	TrackParent osc.NoArg = "/track/parent"

	/*
	    VkbMIDINote(channel uint8, note int8, velocity int8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/note/%d", channel, note)
	   	// int(velocity)


	    VkbMIDIOctaveUp(channel uint8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/note/+", channel)
	   	//


	    VkbMIDIOctaveDown(channel uint8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/note/-", channel)
	   	//


	    VkbMIDIDrumNote(channel uint8, note int8, velocity int8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/drum/%d", channel, note)
	   	// int(velocity)


	    VkbMIDIDrumOctaveUp(channel uint8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/drum/+", channel)
	   	//


	    VkbMIDIDrumOctaveDown(channel uint8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/drum/-", channel)
	   	//


	    VkbMIDIControlChange(channel uint8, cc int8, val int8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/cc/%d", channel, cc)
	   	// int(val)


	    VkbMIDIAfterTouch(channel uint8, note int8, pressure int8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/aftertouch/%d", channel, note)
	   	// int(pressure)


	   // (No-Bend:64)
	    VkbMIDIPitchBend(channel uint8, pitch int8) (msg osc.Message) {
	   	msg.Address = fmt.Sprintf("/vkb_midi/%d/pitchbend", channel)
	   	// int(pitch)


	   // val = 0 -> no fixed velocity, otherwise: set velocity to the val
	    VkbMIDIFixedVelocity(val uint8) (msg osc.Message) {
	   	msg.Address = "/vkb_midi/velocity"
	   	// int(val)

	*/

	// Executes one action from the action list (e.g. Save). Replace spaces in action-ids with a dash (e.g. Select-All).
	Action osc.NoArg = "/action/%s"

	//

	// Flushes all values to the clients
	Refresh osc.NoArg = "/refresh"

	/*
	    Browser Actions
	   • /browser/preset           Activates the browser to browse for presets of the currently selected device
	   • /browser/device           Activates the browser to insert a device after the currently selected device
	   • /browser/device/after (same as /browser/device)
	   • /browser/device/before    Activates the browser to insert a device before the currently selected device
	   • /browser/commit     Commits the current selection in the browser
	   • /browser/cancel     Cancels the current browser session
	   • /browser/filter/{1-6}/{+,-}   The columns are as follows: 1: Location, 2: Favorites, 3: Creator, 4: Tags, 5: Devices, 6: Category
	   • /browser/preset/{+,-}
	*/

	// Activates the browser to browse for presets of the currently selected device
	BrowserPreset osc.NoArg = "/browser/preset"

	// Activates the browser to insert a device after the currently selected device
	BrowserDeviceAfter osc.NoArg = "/browser/device"

	// Activates the browser to insert a device before the currently selected device
	BrowserDeviceBefore osc.NoArg = "/browser/device/before"

	// Commits the current selection in the browser
	BrowserCommit osc.NoArg = "/browser/commit"

	// Cancels the current browser session
	BrowserCancel                  osc.NoArg = "/browser/cancel"
	BrowserFilterLocationNext      osc.NoArg = "/browser/filter/1/+"
	BrowserFilterLocationPrevious  osc.NoArg = "/browser/filter/1/-"
	BrowserFilterFavoritesNext     osc.NoArg = "/browser/filter/2/+"
	BrowserFilterFavoritesPrevious osc.NoArg = "/browser/filter/2/-"
	BrowserFilterCreatorNext       osc.NoArg = "/browser/filter/3/+"
	BrowserFilterCreatorPrevious   osc.NoArg = "/browser/filter/3/-"
	BrowserFilterTagsNext          osc.NoArg = "/browser/filter/4/+"
	BrowserFilterTagsPrevious      osc.NoArg = "/browser/filter/4/-"
	BrowserFilterDevicesNext       osc.NoArg = "/browser/filter/5/+"
	BrowserFilterDevicesPrevious   osc.NoArg = "/browser/filter/5/-"
	BrowserFilterCategoryNext      osc.NoArg = "/browser/filter/6/+"
	BrowserFilterCategoryPrevious  osc.NoArg = "/browser/filter/6/-"
	BrowserPresetNext              osc.NoArg = "/browser/preset/+"
	BrowserPresetPrevious          osc.NoArg = "/browser/preset/-"
)

/*
not implemented yet:

 Cursor Device / Primary Device
• /device/{+,-}

• /device/window        Displays the window for VST plugins

• /device/bypass

• /device/param/{+,-}

• /device/param/{1-8}/value {0-127}

• /device/param/{1-8}/indicate {0,1}

• /device/indicate/param {0,1}

• /device/layer/{1-8}/selected

• /device/layer/{1-8}/volume {0-127}

• /device/layer/{1-8}/pan {0-127}

• /device/layer/{1-8}/mute {1,0,-}

• /device/layer/{1-8}/solo {1,0,-}

• /device/layer/{1-8}/send/{1-8}/volume {0-127}

• /device/layer/{1-8}/enter

• /device/layer/parent

• /device/layer/{+,-}

• /device/layer/page/{+,-}

• Same commands apply for the primary device but use /primary/... instead of /device/...

*/
