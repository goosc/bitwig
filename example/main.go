package main

import (
	"fmt"
	"time"

	"gitlab.com/goosc/bitwig"
	"gitlab.com/goosc/osc"
)

type handler struct{}

func (handler) Matches(p osc.Path) bool {
	return p != "/time/str"
}

func (handler) Handle(path osc.Path, vals ...interface{}) {
	fmt.Printf("bitwig said: %s %v\n", path, vals)
}

func main() {
	bw := bitwig.New(handler{})

	err := bw.Connect()

	if err != nil {
		fmt.Printf("can't connect to bitwig: %#v\n", err.Error())
		return
	}

	fmt.Println(bitwig.TrackRecArm.Set(1 /* track 1 */))
	bitwig.TrackRecArm.Set(1 /* track 1 */).WriteTo(bw, true)

	fmt.Println(bitwig.Click)
	bitwig.Click.WriteTo(bw)

	fmt.Println(bitwig.Play)
	bitwig.Play.WriteTo(bw)
	time.Sleep(time.Second * 3)

	fmt.Println(bitwig.Stop)
	bitwig.Stop.WriteTo(bw)
	time.Sleep(time.Second * 2)
}
