# bitwig
Go library to remote control bitwig studio

[![Documentation](http://godoc.org/github.com/goosc/bitwig?status.png)](http://godoc.org/github.com/goosc/bitwig)

## Installation

It is recommended to use Go 1.11 with module support (`$GO111MODULE=on`).

```
go get -d github.com/goosc/bitwig/...
```

For bitwig studio to be controllable via OSC, you need to install ad enable the OSC control by Jürgen Moßgraber: 

https://github.com/git-moss/DrivenByMoss

Leave the default ports and set the MIDI port to some virtual port and you are ready to go.

## Example

```go
package main

import (
	"fmt"
	"time"

	"github.com/goosc/bitwig"
	"github.com/goosc/osc"
)

type handler struct{}

func (handler) Matches(p osc.Path) bool {
	return p != "/time/str"
}

func (handler) Handle(path osc.Path, vals ...interface{}) {
	fmt.Printf("bitwig said: %s %v\n", path, vals)
}

func main() {
	bw, err := bitwig.Connect(handler{})

	if err != nil {
		fmt.Printf("can't connect to bitwig: %#v\n", err.Error())
		return
	}

	fmt.Println(bitwig.TrackRecArm.Set(1 /* track 1 */))
	bitwig.TrackRecArm.Set(1 /* track 1 */).WriteTo(bw, 1 /* 1 == on */)

	fmt.Println(bitwig.Click)
	bitwig.Click.WriteTo(bw, 1)

	fmt.Println(bitwig.Play)
	bitwig.Play.WriteTo(bw)
	time.Sleep(time.Second * 3)

	fmt.Println(bitwig.Stop)
	bitwig.Stop.WriteTo(bw, 1 /* 1 == do it */)
	time.Sleep(time.Second * 2)
}

```
